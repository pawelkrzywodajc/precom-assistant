// See https://github.com/dialogflow/dialogflow-fulfillment-nodejs
// for Dialogflow fulfillment library docs, samples, and to report issues
'use strict';
 
var options = { day: 'numeric' , month: 'long', hour: 'numeric', minute: 'numeric'};

var jobMap = new Map();

const functions = require('firebase-functions');
const {WebhookClient} = require('dialogflow-fulfillment');
const {Card, Suggestion} = require('dialogflow-fulfillment');
 
//process.env.DEBUG = 'dialogflow:debug'; // enables lib debugging statements
 
exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
  const agent = new WebhookClient({ request, response });
  console.log('Dialogflow Request headers: ' + JSON.stringify(request.headers));
  console.log('Dialogflow Request body: ' + JSON.stringify(request.body));
 
  function welcome(agent) {
    packages(agent);
  }
 
  function fallback(agent) {
    agent.add(`I didn't understand`);
    agent.add(`I'm sorry, can you try again?`);
  }

  function packages(agent) {
    //agent.add(`You have no packages!`);
    fetchPackagesFromUrl(agent, "https://demotnleu-ci-api.azurewebsites.net/api/v1/context/d48db19a71604c7a8b003a7289e6c5de");
  }
  
  function fetchPackagesFromUrl(agent, url) { 
    var fetchedJob = fetchJob("anything");
    var dateLocale = new Date(fetchedJob.expectedEtaTimeUtc).toLocaleDateString('en-EN', options);
    agent.add(`You have 1 package on the way. Package from: ` + fetchedJob.retailerInfo.name + `, expected ETA ` + dateLocale);
  }
  
  function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }
  
  function fetchJob(jobId) {
    return JSON.parse('{"jobId":"d48db19a-7160-4c7a-8b00-3a7289e6c5de","externalId":"Ext-id-d48db19a-7160-4c7a-8b00-3a7289e6c5de","jobType":"1","tenant":"default","retailer":"default","retailerLogoUri":"string","parcels":[{"content":"string","dimensions":"string","dimensionsUnit":"string","weight":"string","weightUnit":"string"}],"assignedTo":0,"state":0,"jobStatusTypeId":30,"statusReason":{"parentType":"string","parentId":"62b2f6e3-aba5-475d-8e9a-ac05e27831fd","eventId":"7918a015-0a61-4fdd-89af-25cadaaba68e","deviationCodeId":0,"comment":"string","description":"string","createdTimeUtc":"2019-10-22T09:46:16.599Z","textField1":"string","numberField1":0,"dateField1":"2019-10-22T09:46:16.599Z","dateField2":"2019-10-22T09:46:16.599Z","quantity":0},"addressInfo":{"name":"string","latitude":0.0,"longitude":0.0,"information":"string","entryCode":"string","contact":"string","phone":"string","email":"string","address1":"string","address2":"string","city":"string"},"etaFromTimeUtc":"2019-10-30T09:46:16.599Z","etaToTimeUtc":"2019-10-30T09:46:16.599Z","expectedEtaTimeUtc":"2019-10-30T09:46:16.599Z","retailerInfo":{"name":"Amazon","latitude":0.0,"longitude":0.0,"information":"string","entryCode":"string","contact":"string","phone":"string","email":"string","address1":"string","address2":"string","city":"string"},"terminalInfo":{"name":"string","latitude":0.0,"longitude":0.0,"information":"string","entryCode":"string","contact":"string","phone":"string","email":"string","address1":"string","address2":"string","city":"string"},"servicePointInfo":{"name":"string","latitude":0.0,"longitude":0.0,"information":"string","entryCode":"string","contact":"string","phone":"string","email":"string","address1":"string","address2":"string","city":"string"},"createdTimeUtc":"2019-10-22T09:46:16.599Z","updatedTimeUtc":"2019-10-22T14:46:16.599Z","completedTimeUtc":"2019-10-22T09:46:16.599Z"}');
  }
  
  function randomETA() {
    return addMinutes(new Date(), 60).toISOString();
  }
  
  function addMinutes(date, minutes) {
      return new Date(date.getTime() + minutes * 60000);
  }
  
  // Run the proper function handler based on the matched Dialogflow intent name
  let intentMap = new Map();
  intentMap.set('Default Welcome Intent', welcome);
  intentMap.set('Default Fallback Intent', fallback);
  intentMap.set('Where are my packages', packages);
  // intentMap.set('your intent name here', yourFunctionHandler);
  // intentMap.set('your intent name here', googleAssistantHandler);
  agent.handleRequest(intentMap);
});
